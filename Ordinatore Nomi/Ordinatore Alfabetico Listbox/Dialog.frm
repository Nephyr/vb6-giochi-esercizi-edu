VERSION 5.00
Begin VB.Form Dialog 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Esporta Lista Nomi"
   ClientHeight    =   3735
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   7335
   Icon            =   "Dialog.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   3735
   ScaleWidth      =   7335
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000004&
      BorderStyle     =   0  'None
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   3
      Text            =   "..."
      Top             =   3320
      Width           =   4215
   End
   Begin VB.DirListBox Dir1 
      Appearance      =   0  'Flat
      Height          =   3015
      Left            =   1200
      TabIndex        =   2
      Top             =   120
      Width           =   6015
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Annulla"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   4440
      TabIndex        =   1
      Top             =   3240
      Width           =   1335
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "Salva"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5880
      TabIndex        =   0
      Top             =   3240
      Width           =   1335
   End
   Begin VB.Image Image3 
      Height          =   720
      Left            =   240
      MouseIcon       =   "Dialog.frx":8934
      MousePointer    =   99  'Custom
      Picture         =   "Dialog.frx":8C3E
      ToolTipText     =   " Cartella Principale di WINDOWS "
      Top             =   2280
      Width           =   720
   End
   Begin VB.Image Image2 
      Height          =   720
      Left            =   240
      MouseIcon       =   "Dialog.frx":A780
      MousePointer    =   99  'Custom
      Picture         =   "Dialog.frx":AA8A
      ToolTipText     =   " Cartella Utenze Condivise "
      Top             =   1240
      Width           =   720
   End
   Begin VB.Image Image1 
      Height          =   720
      Left            =   240
      MouseIcon       =   "Dialog.frx":C5CC
      MousePointer    =   99  'Custom
      Picture         =   "Dialog.frx":C8D6
      ToolTipText     =   " Cartella Utenza Privata "
      Top             =   240
      Width           =   720
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   3015
      Left            =   120
      Top             =   120
      Width           =   975
   End
End
Attribute VB_Name = "Dialog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CancelButton_Click()

  Dialog.Hide

End Sub

Private Sub Dir1_Change()

  Text1.Text = Dir1
  Text1.ToolTipText = Dir1

End Sub

Private Sub Form_Load()

  Dir1 = Environ("HOMEDRIVE")
  Text1.Text = Environ("HOMEDRIVE")

End Sub

Private Sub Image1_Click()

  Dir1 = Environ("HOMEPATH")
  Text1.Text = Environ("HOMEDRIVE") & Environ("HOMEPATH")

End Sub

Private Sub Image2_Click()

  Dir1 = Environ("ALLUSERSPROFILE")
  Text1.Text = Environ("ALLUSERSPROFILE")
  
End Sub

Private Sub Image3_Click()

  Dir1 = Environ("SYSTEMROOT")
  Text1.Text = Environ("SYSTEMROOT")
  
End Sub

Private Sub OKButton_Click()

  Dim Vet_Name(30) As String
  
  For i = 0 To Form1.List1.ListCount - 1 Step 1
     Vet_Name(i) = Form1.List1.List(i)
  Next

  Open Dir1 & "/Lista_Nomi_Ordinati.txt" For Output Access Write As 1
  Print #1, vbCrLf & " Lista Nomi per Ordine Alfabetico." & vbCrLf & "----------------------------------------------------------------------------------------" & vbCrLf
  For i = 0 To Form1.List1.ListCount - 1 Step 1
     If i < 10 Then
       Print #1, "  " & i & " - " & Vet_Name(i)
     Else
       Print #1, " " & i & " - " & Vet_Name(i)
     End If
  Next
  Print #1, vbCrLf & "----------------------------------------------------------------------------------------" & vbCrLf & " By Alphabetic Sort - Program." & vbCrLf & " Copyright - Vanzo Luca Samuele."
  Close #1
  tmp = MsgBox(" Lista Salvata correttamente sul computer in uso ", vbInformation)
  Dialog.Hide

End Sub
