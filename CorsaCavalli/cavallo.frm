VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Corsa dei Cavalli"
   ClientHeight    =   7620
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11055
   Icon            =   "cavallo.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   Picture         =   "cavallo.frx":1294B
   ScaleHeight     =   7620
   ScaleWidth      =   11055
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txt_euro_poss 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   2180
      TabIndex        =   6
      Text            =   "10000"
      Top             =   7200
      Width           =   2880
   End
   Begin VB.TextBox txt_ordine 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   8060
      TabIndex        =   5
      Top             =   7200
      Width           =   2880
   End
   Begin VB.TextBox txt_euro 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Index           =   1
      Left            =   7050
      TabIndex        =   4
      Top             =   120
      Width           =   1695
   End
   Begin VB.CommandButton cmd_gara 
      Caption         =   "avvia gara"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   8880
      TabIndex        =   1
      Top             =   120
      Width           =   2055
   End
   Begin VB.TextBox txt_cavallo 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   3130
      TabIndex        =   0
      Top             =   120
      Width           =   1680
   End
   Begin VB.Timer corsa_left 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   10680
      Top             =   600
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Cavallo in corsa primo ="
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   3
      Left            =   5040
      TabIndex        =   8
      Top             =   7200
      Width           =   3015
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Euro in possesso ="
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   2
      Left            =   120
      TabIndex        =   7
      Top             =   7200
      Width           =   2055
   End
   Begin VB.Line Line2 
      X1              =   0
      X2              =   11040
      Y1              =   7040
      Y2              =   7040
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "= la somma di Euro ="
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   1
      Left            =   4800
      TabIndex        =   3
      Top             =   120
      Width           =   2295
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      Caption         =   "Puntare sul cavallo numero ="
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   255
      Index           =   0
      Left            =   0
      TabIndex        =   2
      Top             =   120
      Width           =   3135
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      Height          =   325
      Index           =   29
      Left            =   10320
      Top             =   6720
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   325
      Index           =   28
      Left            =   10680
      Top             =   6720
      Width           =   375
   End
   Begin VB.Image cavallo 
      Height          =   1500
      Index           =   3
      Left            =   120
      Picture         =   "cavallo.frx":124EFD
      Top             =   5400
      Width           =   795
   End
   Begin VB.Image cavallo 
      Height          =   1500
      Index           =   2
      Left            =   120
      Picture         =   "cavallo.frx":126127
      Top             =   3840
      Width           =   795
   End
   Begin VB.Image cavallo 
      Height          =   1500
      Index           =   1
      Left            =   120
      Picture         =   "cavallo.frx":127351
      Top             =   2260
      Width           =   795
   End
   Begin VB.Image cavallo 
      Height          =   1500
      Index           =   0
      Left            =   120
      Picture         =   "cavallo.frx":12857B
      Top             =   690
      Width           =   795
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      Height          =   6525
      Index           =   27
      Left            =   960
      Top             =   520
      Width           =   60
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   6495
      Index           =   26
      Left            =   1005
      Top             =   540
      Width           =   45
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   25
      Left            =   10320
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   24
      Left            =   10680
      Top             =   5760
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   23
      Left            =   10320
      Top             =   5280
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   22
      Left            =   10680
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   21
      Left            =   10320
      Top             =   4320
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   20
      Left            =   10680
      Top             =   3840
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   19
      Left            =   10320
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   18
      Left            =   10680
      Top             =   2880
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   17
      Left            =   10320
      Top             =   2400
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   16
      Left            =   10680
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   15
      Left            =   10320
      Top             =   1440
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   14
      Left            =   10320
      Top             =   540
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00000000&
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   13
      Left            =   10680
      Top             =   960
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   12
      Left            =   10680
      Top             =   6240
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   11
      Left            =   10320
      Top             =   5760
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   10
      Left            =   10680
      Top             =   5280
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   9
      Left            =   10320
      Top             =   4800
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   8
      Left            =   10680
      Top             =   4320
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   7
      Left            =   10320
      Top             =   3840
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   6
      Left            =   10680
      Top             =   3360
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   5
      Left            =   10320
      Top             =   2880
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   4
      Left            =   10680
      Top             =   2400
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   3
      Left            =   10320
      Top             =   1920
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   2
      Left            =   10680
      Top             =   1440
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   1
      Left            =   10320
      Top             =   960
      Width           =   375
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  'Opaque
      Height          =   495
      Index           =   0
      Left            =   10680
      Top             =   540
      Width           =   375
   End
   Begin VB.Line Line1 
      X1              =   0
      X2              =   11040
      Y1              =   530
      Y2              =   530
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Function result()

   Rem invio dei risultati e in base a vincita / perdita eseguo l'operazioni adeguata
   Form2.lbl_cav(0).Caption = "Vincente N�" & txt_ordine(1).Text
   Form2.lbl_cav(1).Caption = "Scommesso N�" & txt_cavallo(0).Text
   
   If txt_cavallo(0).Text = txt_ordine(1).Text Then
   
      Form2.operazione(1).Caption = "Accredito - Euro"
      
      Form2.res(1).Text = txt_euro_poss(2).Text
      Form2.res(2).Text = Abs(txt_euro(1).Text * 2)
      txt_euro_poss(2).Text = Form2.res(1).Text + Abs(txt_euro(1).Text * 2)
      Form2.res(0).Text = txt_euro_poss(2).Text
      
   Else
   
      Form2.operazione(1).Caption = "Prelievo - Euro"
      
      Form2.res(1).Text = txt_euro_poss(2).Text
      Form2.res(2).Text = Abs((txt_euro(1).Text))
      txt_euro_poss(2).Text = Form2.res(1).Text - Abs(txt_euro(1).Text)
      Form2.res(0).Text = txt_euro_poss(2).Text
   
   End If

End Function

Private Sub cmd_gara_Click()
 
 Rem Avvia la gara dei cavalli dopo aver verificato la validit� dei dati immessi dall'utente
 Rem Se il numero scelto per il cavallo non � entro l'intervallo prestabilito stampa un errore
 Rem Se gli euro immessi non sono disponibili o pari a zero stampa un errore, se minori o uguali a zero chiede all'utente cosa fare
 
 If Val(txt_cavallo(0).Text) <> 0 Then
   
   If txt_cavallo(0).Text <= 4 Then
   
     If (Val(txt_euro(1).Text) > txt_euro_poss(2).Text) Or (Val(txt_euro(1).Text) = 0) Then
   
       MsgBox ("ERRORE - Somma immessa attualmente non disponibile o pari a zero...")
       
       If txt_euro_poss(2).Text <= 0 Then
       
         Dim res_euro As Integer
         res_euro = MsgBox("Euro insufficienti. Si vuole ricominciare con 10000 Euro?", vbYesNo)
         
         If res_euro = vbYes Then
         
            txt_euro_poss(2).Text = 10000
         
         Else
            
            Cancel = True
         
         End If
       
       End If
      
     Else
   
       corsa_left.Enabled = True
       txt_cavallo(0).Enabled = False
       txt_euro(1).Enabled = False
       cmd_gara.Enabled = False
       
     End If
   
   Else
   
     MsgBox ("ERRORE - Selezionare un cavallo entro: 1 e 4...")
   
   End If
   
 Else
 
   MsgBox ("Cavallo, scommessa o entrambi non impostati...")
 
 End If

End Sub

Private Sub corsa_left_Timer()
   
   Rem media per diminuire le probabilit� di ripetizioni numeriche
   
   num1 = Fix(Rnd() * 25 + 1)
   num2 = Fix(Rnd() * 25 + 1)
   num3 = Fix(Rnd() * 25 + 1)
   num4 = Fix(Rnd() * 25 + 1)
   
   num = Fix((num1 + num2 + num3 + num4) / 4)
   
   Rem Cavallo numero 1 - Impostazioni
   Rem Se il cavallo raggiunge il massimo consentito (9600) ferma il timer
   If cavallo(0).Left >= 9600 Then
   
      corsa_left.Enabled = False
      Form2.Show
      Form1.Enabled = False
      
      Call result
      
   Else
      
      cavallo(0).Left = cavallo(0).Left + Fix(Rnd() * 6 + 1) * num
      
   End If
   
   Rem Cavallo numero 2 - Impostazioni
   Rem Se il cavallo raggiunge il massimo consentito (9600) ferma il timer
   If cavallo(1).Left >= 9600 Then
   
      corsa_left.Enabled = False
      Form2.Show
      Form1.Enabled = False
      
      Call result
      
   Else
      
      cavallo(1).Left = cavallo(1).Left + Fix(Rnd() * 6 + 1) * num
      
   End If
   
   Rem Cavallo numero 3 - Impostazioni
   Rem Se il cavallo raggiunge il massimo consentito (9600) ferma il timer
   If cavallo(2).Left >= 9600 Then
   
      corsa_left.Enabled = False
      Form2.Show
      Form1.Enabled = False
      
      Call result
      
   Else
      
      cavallo(2).Left = cavallo(2).Left + Fix(Rnd() * 6 + 1) * num
      
   End If
   
   Rem Cavallo numero 4 - Impostazioni
   Rem Se il cavallo raggiunge il massimo consentito (9600) ferma il timer
   If cavallo(3).Left >= 9600 Then
   
      corsa_left.Enabled = False
      Form2.Show
      Form1.Enabled = False
      
      Call result
      
   Else
      
      cavallo(3).Left = cavallo(3).Left + Fix(Rnd() * 6 + 1) * num
      
   End If
   
   
   Rem Tramite confronti ricavo il Cavallo in testa alla gara e lo assegno a txt_ordine(1)
   If (cavallo(0).Left > cavallo(1).Left) And (cavallo(0).Left > cavallo(2).Left) And (cavallo(0).Left > cavallo(3).Left) Then
   
      txt_ordine(1).Text = "1"
   
   End If
   
   If (cavallo(1).Left > cavallo(0).Left) And (cavallo(1).Left > cavallo(2).Left) And (cavallo(1).Left > cavallo(3).Left) Then
   
      txt_ordine(1).Text = "2"
   
   End If
   
   If (cavallo(2).Left > cavallo(1).Left) And (cavallo(2).Left > cavallo(0).Left) And (cavallo(2).Left > cavallo(3).Left) Then
   
      txt_ordine(1).Text = "3"
   
   End If
   
   If (cavallo(3).Left > cavallo(1).Left) And (cavallo(3).Left > cavallo(2).Left) And (cavallo(3).Left > cavallo(0).Left) Then
   
      txt_ordine(1).Text = "4"
   
   End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Risp As Integer
    Risp = MsgBox("Sei sicuro di voler chiudere il gioco?", vbYesNo)
    If Risp = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub
