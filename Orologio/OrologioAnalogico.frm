VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Orologio Analogico"
   ClientHeight    =   4815
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7575
   Icon            =   "OrologioAnalogico.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4815
   ScaleWidth      =   7575
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame obj_option 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Impostazioni Orologio "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4650
      Left            =   2880
      TabIndex        =   16
      Top             =   50
      Width           =   4575
      Begin VB.CommandButton Command5 
         Caption         =   "Annulla"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   34
         Top             =   4150
         Width           =   1215
      End
      Begin VB.Frame Frame6 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Misura Lancette "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1440
         Left            =   120
         TabIndex        =   27
         Top             =   2500
         Width           =   4335
         Begin VB.TextBox txt_mis_sec 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   30
            Text            =   "1900"
            Top             =   310
            Width           =   2175
         End
         Begin VB.TextBox txt_mis_min 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   29
            Text            =   "1600"
            Top             =   660
            Width           =   2175
         End
         Begin VB.TextBox txt_mis_ore 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   28
            Text            =   "1200"
            Top             =   1010
            Width           =   2175
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Lancetta Secondi"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   33
            Top             =   360
            Width           =   1815
         End
         Begin VB.Label Label6 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Lancetta Minuti"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   32
            Top             =   690
            Width           =   1815
         End
         Begin VB.Label Label7 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "Lancetta Ore"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   31
            Top             =   1030
            Width           =   1815
         End
      End
      Begin VB.Frame Frame5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Sfasamento Minuti / Secondi Orologio "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   1125
         Left            =   120
         TabIndex        =   22
         Top             =   1170
         Width           =   4335
         Begin VB.TextBox txt_min_sfas 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   300
            Left            =   2040
            TabIndex        =   24
            Text            =   "0"
            Top             =   645
            Width           =   2175
         End
         Begin VB.TextBox txt_sec_sfas 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   23
            Text            =   "0"
            Top             =   240
            Width           =   2175
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            Caption         =   "Imposta Minuti"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   680
            Width           =   1815
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H80000005&
            Caption         =   "Imposta Secondi"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   260
            Width           =   1815
         End
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Default"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1560
         TabIndex        =   21
         Top             =   4150
         Width           =   1335
      End
      Begin VB.Frame Frame4 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         Caption         =   "Sfasamento Temporale "
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   6.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   4335
         Begin VB.TextBox sfas_temp 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            TabIndex        =   19
            Text            =   "1000"
            Top             =   260
            Width           =   2175
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "In millisecondi"
            BeginProperty Font 
               Name            =   "Arial"
               Size            =   9.75
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   270
            Width           =   1815
         End
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Imposta"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3000
         TabIndex        =   17
         Top             =   4150
         Width           =   1335
      End
      Begin VB.Label lbl_Min_Sfas 
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         Height          =   255
         Left            =   2880
         TabIndex        =   39
         Top             =   1200
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lbl_Sec_Sfas 
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         Height          =   255
         Left            =   2160
         TabIndex        =   38
         Top             =   1200
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.Label lbl_ore 
         Caption         =   "1200"
         Height          =   255
         Left            =   3960
         TabIndex        =   37
         Top             =   2520
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label lbl_min 
         Caption         =   "1600"
         Height          =   255
         Left            =   3360
         TabIndex        =   36
         Top             =   2520
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Label lbl_sec 
         Caption         =   "1900"
         Height          =   255
         Left            =   2760
         TabIndex        =   35
         Top             =   2520
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Line Line1 
         X1              =   120
         X2              =   4440
         Y1              =   4050
         Y2              =   4050
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Sec | Min | Ore - RAD. "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2175
      Left            =   120
      TabIndex        =   7
      Top             =   1200
      Width           =   2655
      Begin VB.TextBox ora_prog 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   480
         Width           =   2415
      End
      Begin VB.TextBox ora_prog 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   2415
      End
      Begin VB.TextBox ora_prog 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   120
         TabIndex        =   8
         Top             =   1680
         Width           =   2415
      End
      Begin VB.Label min_pgreco 
         BackStyle       =   0  'Transparent
         Caption         =   "0"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   840
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label sec_pgreco 
         BackStyle       =   0  'Transparent
         Caption         =   "-360"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Visible         =   0   'False
         Width           =   375
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Secondi"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   2415
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Minuti"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   12
         Top             =   840
         Width           =   2415
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Ore"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   11
         Top             =   1440
         Width           =   2415
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Ora Sys "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1045
      Left            =   120
      TabIndex        =   4
      Top             =   50
      Width           =   2655
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   0
         Top             =   0
      End
      Begin VB.TextBox txt_ora_sys 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   120
         TabIndex        =   5
         Top             =   500
         Width           =   2415
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Ora del sistema"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   2415
      End
   End
   Begin VB.Frame Frame3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      Caption         =   "Esegui "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   120
      TabIndex        =   1
      Top             =   3480
      Width           =   2655
      Begin VB.CommandButton Command1 
         Caption         =   "Ferma Orologio"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   2415
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Impostazioni"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   2415
      End
   End
   Begin VB.PictureBox pict 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4575
      Left            =   2880
      ScaleHeight     =   4575
      ScaleWidth      =   4575
      TabIndex        =   0
      Top             =   120
      Width           =   4575
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Function ind_sec(sec, min, ore)

       pict.Cls
       pict.DrawWidth = 1
       
       Rem Assegno alle variabile i valori del PictureBox
       h = -4575
       w = 4575
       l = -2288
       t = 2288
     
       Rem Imposto il PictureBox in base alle assegnazioni precedenti
       pict.ScaleMode = 0
       pict.ScaleHeight = h
       pict.ScaleWidth = w
       pict.ScaleLeft = l
       pict.ScaleTop = t
       
       sec_mis = lbl_sec.Caption
       min_mis = lbl_min.Caption
       ore_mis = lbl_ore.Caption
  
       Rem Traccio gli assi di riferimento X,Y
       pict.Line (0, -2000)-(0, -1900), vbBlack
       pict.Line (0, 1900)-(0, 2000), vbBlack
       pict.Line (-1900, 0)-(-2000, 0), vbBlack
       pict.Line (1900, 0)-(2000, 0), vbBlack
       pict.Circle (0, 0), 2100, vbBlack
       
       Rem lancetta sec
       pict.DrawWidth = 1
       pict.Line (0, 0)-(sec_mis * Sin((-sec * 6.28) / 360), sec_mis * Cos((-sec * 6.28) / 360)), vbGreen
       
       Rem lancetta min
       pict.DrawWidth = 3
       pict.Line (0, 0)-(min_mis * Sin((-min * 6.28) / 360), min_mis * Cos((-min * 6.28) / 360)), vbBlue
       
       Rem lancetta ore
       pict.DrawWidth = 5
       pict.Line (0, 0)-(ore_mis * Sin((-ore * 6.28) / 360), ore_mis * Cos((-ore * 6.28) / 360)), vbRed
       
       pict.FillColor = vbBlack
       pict.Circle (0, 0), 45, vbRed

End Function

Private Sub Command1_Click()

       If Command1.Caption = "Ferma Orologio" Then
       
          Rem fermo il timer
          Timer1.Enabled = False
          Command1.Caption = "Avvia Orologio"
       
       Else
       
          Rem avvio il timer
          Timer1.Enabled = True
          Command1.Caption = "Ferma Orologio"
       
       End If

End Sub

Private Sub Command2_Click()

       Rem mostro le impostazioni
       obj_option.Visible = True

End Sub

Private Sub Command3_Click()

       Rem nascondo le impostazioni
       obj_option.Visible = False
       
       Rem imposto le scelte
       Timer1.Interval = Val(sfas_temp.Text)
       
       Rem dimensione lancette
       Lanc_Sec = Val(txt_mis_sec.Text)
       Lanc_min = Val(txt_mis_min.Text)
       Lanc_ore = Val(txt_mis_ore.Text)
       If Lanc_Sec < 2000 Then
          If Lanc_Sec <> 0 Then
            lbl_sec.Caption = txt_mis_sec.Text
          Else
            lbl_sec.Caption = 1900
            txt_mis_sec.Text = 1900
          End If
       Else
          lbl_sec.Caption = 1900
          txt_mis_sec.Text = 1900
       End If
       
       If Lanc_min < 2000 Then
          If Lanc_min <> 0 Then
            lbl_min.Caption = txt_mis_min.Text
          Else
            lbl_min.Caption = 1600
            txt_mis_min.Text = 1600
          End If
       Else
          lbl_min.Caption = 1600
          txt_mis_min.Text = 1600
       End If
       
       If Lanc_ore < 2000 Then
          If Lanc_ore <> 0 Then
            lbl_ore.Caption = txt_mis_ore.Text
          Else
            lbl_ore.Caption = 1200
            txt_mis_ore.Text = 1200
          End If
       Else
          lbl_ore.Caption = 1200
          txt_mis_ore.Text = 1200
       End If
       
       If Val(txt_sec_sfas.Text) < 61 Then
          If Val(txt_sec_sfas.Text) >= 0 Then
             lbl_Sec_Sfas.Caption = Val(txt_sec_sfas.Text)
          Else
             lbl_Sec_Sfas.Caption = 0
             txt_sec_sfas.Text = 0
          End If
       Else
          lbl_Sec_Sfas.Caption = 0
          txt_sec_sfas.Text = 0
       End If
       
       If Val(txt_min_sfas.Text) < 61 Then
          If Val(txt_min_sfas.Text) >= 0 Then
             lbl_Min_Sfas.Caption = Val(txt_min_sfas.Text)
          Else
             lbl_Min_Sfas.Caption = 0
             txt_min_sfas.Text = 0
          End If
       Else
          lbl_Min_Sfas.Caption = 0
          txt_min_sfas.Text = 0
       End If

End Sub

Private Sub Command4_Click()

       Rem imposto il default
       sfas_temp.Text = 1000
       txt_sec_sfas.Text = 0
       txt_min_sfas.Text = 0
       
       txt_mis_sec.Text = 1900
       txt_mis_min.Text = 1600
       txt_mis_ore.Text = 1200
       
End Sub

Private Sub Command5_Click()

       Rem nascondo le impostazioni
       obj_option.Visible = False

End Sub

Private Sub Form_Load()
       
       Rem nascondo le impostazioni
       obj_option.Visible = False
       
       Rem avvio il timer
       Timer1.Interval = Val(sfas_temp.Text)
       Timer1.Enabled = True

End Sub

Private Sub Timer1_Timer()
       
       Rem ricavo i secondi, minuti e ore del sistema convertendoli in radianti
       sec = Second(Now()) + lbl_Sec_Sfas.Caption
       min = Minute(Now()) + lbl_Min_Sfas.Caption
       ore = Hour(Now()) * 5
       
       Rem ricavo ora di sistema hh:mm:ss
       txt_ora_sys.Text = Hour(Now()) & "  :  " & min & "  :  " & sec
       
       Rem assegno il corrispondente in radianti dei secondi, minuti e ore attuali
       ora_prog(0).Text = Fix(0 - (sec * 6))
       ora_prog(1).Text = Fix(0 - (min * 6))
       ora_prog(2).Text = Fix(0 - ore * 6 - 30 * min / 60)
          
       Rem chiamo la funzione adibita al disegno dell'orologio e lancette
       Call ind_sec(ora_prog(0).Text, ora_prog(1).Text, ora_prog(2).Text)

End Sub
