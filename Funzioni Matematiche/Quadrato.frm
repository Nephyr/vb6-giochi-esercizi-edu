VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   1425
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4215
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1425
   ScaleWidth      =   4215
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Pulisci"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   1080
      Width           =   1815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Calcola"
      Height          =   255
      Left            =   2160
      TabIndex        =   2
      Top             =   1080
      Width           =   1815
   End
   Begin VB.TextBox txt_res 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Index           =   1
      Left            =   1920
      TabIndex        =   1
      Top             =   480
      Width           =   2175
   End
   Begin VB.TextBox txt_num 
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Index           =   0
      Left            =   1920
      TabIndex        =   0
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Valore Iniz."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   0
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Risultato"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   1
      Left            =   120
      TabIndex        =   4
      Top             =   480
      Width           =   1695
   End
   Begin VB.Line Line1 
      X1              =   120
      X2              =   4080
      Y1              =   960
      Y2              =   960
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()

  Dim quadrato, cont As Long
  
  txt_num(0).Text = ""
  txt_res(1).Text = ""
  
  Inc = InputBox("Inserire il numero da elevare...", "Calcola Quadrato")
  
  If Inc = 0 Then
  
    txt_num(0).Text = "Impossibile Calcolare..."
    txt_res(1).Text = "Impossibile Calcolare..."
  
  Else
  
    cont = 1
    quadrato = 1
    
    While cont <= Inc

        quadrato = quadrato + 2
        cont = cont + 1
    
    Wend
    
    txt_num(0).Text = Inc
    txt_res(1).Text = quadrato

  End If

End Sub

Private Sub Command2_Click()

  txt_num(0).Text = ""
  txt_res(1).Text = ""

End Sub
