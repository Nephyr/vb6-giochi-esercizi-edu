VERSION 5.00
Begin VB.Form Form1 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Funzione Retta :: ax + by + c = 0"
   ClientHeight    =   5220
   ClientLeft      =   285
   ClientTop       =   285
   ClientWidth     =   8685
   LinkTopic       =   "Funzione Retta"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5220
   ScaleWidth      =   8685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Caption         =   "Esito "
      ForeColor       =   &H80000008&
      Height          =   855
      Left            =   240
      TabIndex        =   13
      Top             =   2400
      Width           =   3255
      Begin VB.TextBox txt_esito 
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   525
         Left            =   120
         MultiLine       =   -1  'True
         TabIndex        =   14
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.TextBox txt_y 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   2400
      TabIndex        =   12
      Top             =   3720
      Width           =   1095
   End
   Begin VB.TextBox txt_yneg 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   11
      Top             =   3720
      Width           =   1095
   End
   Begin VB.TextBox txt_x 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   2400
      TabIndex        =   10
      Top             =   3360
      Width           =   1095
   End
   Begin VB.TextBox txt_xneg 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Enabled         =   0   'False
      Height          =   285
      Left            =   240
      TabIndex        =   9
      Top             =   3360
      Width           =   1095
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Pulisci"
      Height          =   375
      Left            =   300
      TabIndex        =   8
      Top             =   4200
      Width           =   3105
   End
   Begin VB.TextBox txt_c 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   240
      TabIndex        =   4
      Top             =   1920
      Width           =   3255
   End
   Begin VB.TextBox txt_b 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   240
      TabIndex        =   3
      Top             =   1200
      Width           =   3255
   End
   Begin VB.TextBox txt_a 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   240
      TabIndex        =   2
      Top             =   480
      Width           =   3255
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Visualizza"
      Height          =   375
      Left            =   315
      TabIndex        =   1
      Top             =   4680
      Width           =   3090
   End
   Begin VB.PictureBox obj_pict 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   5000
      Left            =   3600
      ScaleHeight     =   -5220.82
      ScaleLeft       =   2500
      ScaleMode       =   0  'User
      ScaleTop        =   -2500
      ScaleWidth      =   5030.395
      TabIndex        =   0
      Top             =   120
      Width           =   5000
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "< Y <"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   4
      Left            =   1320
      TabIndex        =   16
      Top             =   3720
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "< X <"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Index           =   3
      Left            =   1320
      TabIndex        =   15
      Top             =   3360
      Width           =   1095
   End
   Begin VB.Shape Shape1 
      Height          =   3975
      Left            =   120
      Top             =   120
      Width           =   3495
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Valore C"
      Height          =   255
      Index           =   2
      Left            =   240
      TabIndex        =   7
      Top             =   1680
      Width           =   2535
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Valore B"
      Height          =   255
      Index           =   1
      Left            =   240
      TabIndex        =   6
      Top             =   960
      Width           =   2535
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Valore A"
      Height          =   255
      Index           =   0
      Left            =   240
      TabIndex        =   5
      Top             =   240
      Width           =   2535
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Function rettax(a, b, c)

   If b <> 0 And a <> 0 Then
   rettax = -(b / a) + -(c / a)
   End If
   
   If b = 0 Then
   rettax = -(c / a)
   End If
   
   If a = 0 Then
   rettax = 0
   End If
   
   If c = 0 Then
   rettax = -(b / a)
   End If

End Function

Private Function rettay(c, a, b)

   If a <> 0 And b <> 0 Then
   rettay = -(a / b) + -(c / b)
   End If
   
   If a = 0 Then
   rettay = -(c / b)
   End If
   
   If b = 0 Then
   rettay = 0
   End If
   
   If c = 0 Then
   rettay = -(a / b)
   End If

End Function

Private Sub Command1_Click()

     Rem Dichiaro le variabili e ne assegno il valore del campo testuale
     Dim a, b, c, h, w, l, t As Single
     a = Val(txt_a.Text)
     b = Val(txt_b.Text)
     c = Val(txt_c.Text)
     
     Rem Pulisco il PictureBox
     obj_pict.Cls
     
     Rem Se pi� di due valori sono impostati a zero o sono alfabetici(Val = 0)
     If (a = 0 And b = 0) Or (a = 0 And c = 0) Or (b = 0 And c = 0) Or (a = 0 And b = 0 And c = 0) Then
       
       Rem Scrivo nel campo testuale l'errore commesso ed imposto a ZERO gli intervalli
       txt_esito.Text = "Errore: Impossibile calcolare con 2 o pi� valori nulli o alfabetici"
       txt_xneg.Text = 0
       txt_x.Text = 0
       txt_yneg.Text = 0
       txt_y.Text = 0
     
     Else Rem altrimenti proseguo
     
       Rem Assegno alla variabile X la funzione rettax
       X = Fix(rettax(a, b, c))
       Rem Assegno alla variabile Y la funzione rettay
       Y = Fix(rettay(c, a, b))
     
       If (Abs(X) > Abs(Y)) And (Abs(X) = Abs(Y)) Then
       
        Temp = X
        
       Else
       
        Temp = Y
        
       End If
       
       Rem Assegno alle variabile i valori del PictureBox
        h = obj_pict.Height
        w = obj_pict.Width
        l = h / 2
        t = w / 2
        
        Rem Imposto il PictureBox in base alle assegnazioni precedenti
        obj_pict.ScaleMode = 0
        obj_pict.ScaleHeight = -h
        obj_pict.ScaleWidth = w
        obj_pict.ScaleLeft = -l
        obj_pict.ScaleTop = t
       
       Rem Traccio gli assi di riferimento X,Y
       obj_pict.Line (0, -l)-(0, t), &HE0E0E0
       obj_pict.Line (-l, 0)-(t, 0), &HE0E0E0
     
       Rem Disegno la retta corrispondente: B = 0
       If b = 0 Then
       obj_pict.Line (X, -2500)-(X, 2500), vbRed
       txt_xneg.Text = X
       txt_x.Text = X
       txt_yneg.Text = "Parallela"
       txt_y.Text = "Parallela"
       End If
     
       Rem Disegno la retta corrispondente: A = 0
       If a = 0 Then
       obj_pict.Line (-2500, Y)-(2500, Y), vbRed
       txt_xneg.Text = "Parallela"
       txt_x.Text = "Parallela"
       txt_yneg.Text = Y
       txt_y.Text = Y
       End If
     
       Rem Disegno la retta corrispondente: (A, B, C != 0) oppure (C = 0)
       If (a <> 0 And b <> 0 And c <> 0) Or (c = 0) Then
       For i = -2500 To 2500
       
          obj_pict.PSet (i + rettax(c, a, b), i + rettay(c, a, b)), vbRed
       
       Next
       txt_xneg.Text = Fix(-2500 + rettax(c, a, b))
       txt_x.Text = Fix(2500 + rettax(c, a, b))
       txt_yneg.Text = Fix(-2500 + rettax(c, a, b))
       txt_y.Text = Fix(2500 + rettax(c, a, b))
       End If
     
     
       Rem Se il calcolo � stato eseguito senza errori scrivo nel campo testuale
       txt_esito.Text = "Completato con successo"

     
     End If
     

End Sub

Private Sub Command2_Click()

    obj_pict.Cls
    txt_xneg.Text = ""
    txt_x.Text = ""
    txt_yneg.Text = ""
    txt_y.Text = ""
    txt_a.Text = ""
    txt_b.Text = ""
    txt_c.Text = ""
    txt_esito.Text = ""

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Dim Risp As Integer
    Risp = MsgBox("Sei sicuro di voler chiudere il programma?", vbYesNo)
    If Risp = vbNo Then
        Cancel = True
    Else
        End
    End If
End Sub
